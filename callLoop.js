const fetchApi = require('./fetchconfig');

const getLoop = async function callLoop(connection, idClasroom, idContentGroup) {

    let chapterNContent = {
        idContentArray: [],
        idChapterArray: []
    }
    let idContentTemp = "";

    return new Promise(function (resolve,reject) {
        connection.query(`
        SELECT * FROM mdl_course`,
        (err, results) => {
        if (err) throw err;

        results.forEach(async (element) => {
            let contentMutation = `
            {
              createContent(classroomId: ${idClasroom}, input: {
                category: 1,
                contentgroupId: ${idContentGroup},
                description: "${element.summary}",
                hidden: false,
                name: "${element.fullname}",
                options: "${JSON.stringify(element)}", 
                order: 1,
                url: "",
                }){
                    id,
                    name
                }
            }`;
        
            const contentQuery = JSON.stringify({ query: `${contentMutation}`});
            //const contentData = await fetchApi(contentQuery);
            //chapterNContent.idContentArray.push(contentData['createContent']);
            chapterNContent.idContentArray.push(contentQuery);
            //idContentTemp = contentData['createContent'].id;
            idContentTemp = 1;


            let chapterMutation = `
            {
            createChapter(classroomId: ${idClasroom}, input: {
                contentId:  ${idContentTemp},
                hidden: 1,
                name: "${element.fullname}",
                option: "${JSON.stringify(element)}", 
                order: 1
                }){
                    id,
                    name
                }
            }`;

            const chapterQuery = JSON.stringify({ query: `${chapterMutation}`});
            //const chapterData = await fetchApi(chapterQuery);
            //chapterNContent.idChapterArray.push(chapterData['createChapter']);0
            chapterNContent.idChapterArray.push(chapterQuery);
        });
    
        resolve(chapterNContent);

      });
    })
}
  
module.exports = getLoop;