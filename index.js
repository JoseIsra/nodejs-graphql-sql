const mysql = require('mysql');
const runPrefix = require('./prefix');
const runWhitoutPrefix = require('./nprefix');
const preMutations = require('./preMutations');
const getUser = require('./callUserMoodle');
const getTeachers = require('./callTeachers');


let database_name = 'moodle';

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : `${database_name}`,
});

connection.connect(function(err) {
  if (err) {
    console.error('Error connecting: ' + err.stack);
    return;
  }
  console.log('Connected sucesfully');
});


connection.query(`
  SELECT *
  FROM information_schema.tables
  WHERE table_schema = "${database_name}" AND table_name = 'course'
  LIMIT 1;`,  async (err, results) =>{
  if(err) console.log('error');

  if(results.length == 0){
    let dataUser = await getUser(connection, true);
    let teachers = await getTeachers(connection);
    let data = await preMutations(connection,dataUser,teachers);
    console.log(data);
    //runPrefix(connection, data);
  }else{
    let dataUser = await getUser(connection, false);
    // let data = await preMutations(dataUser);
  }
});


//connection.end();



