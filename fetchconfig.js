const fetch = require('node-fetch');

async function fetchApi(data) {
  const response = await fetch(
    'https://lxp.fractaluptest.xyz/api/graphiql',
    {
      method: 'post',
      body: data,
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': data.length,
        // 'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ilg1ZVhrNHh5b2pORnVtMWtsMll0djhkbE5QNC1jNTdkTzZRR1RWQndhTmsifQ.eyJpc3MiOiJodHRwczovL2ZyYWN0YWx1cC5iMmNsb2dpbi5jb20vNzQzN2UyOWEtOTI3MC00NTAwLTllMDEtN2NmNTk1ZTQzMmNhL3YyLjAvIiwiZXhwIjoxNjIxODg4NzA0LCJuYmYiOjE2MjE4ODY5MDQsImF1ZCI6IjEwYWI1NGU5LTVkNDAtNGUxMS1iMzM5LTY4ZGM0ODM2Zjc5MSIsInN1YiI6IjI5MjU2MWQwLTM5ZWUtNDEwYi1iMTVjLTU2NWVkOWQ0ZTljMCIsImdpdmVuX25hbWUiOiJGYWJyaXppbyIsImZhbWlseV9uYW1lIjoiQWd1aXJyZSIsImV4dGVuc2lvbl9BcGVsbGlkb01hdGVybm8iOiJDcnV6IiwiZXh0ZW5zaW9uX0ROSSI6Nzg5NDU2MTIsImV4dGVuc2lvbl9UZXJtaW5vc3lDb25kaWNpb25lcyI6IkFjZXB0byIsImVtYWlscyI6WyJmYWd1aXJyZUBmcmFjdGFsdXAuY29tIl0sInRmcCI6IkIyQ18xX2x4cCIsIm5vbmNlIjoiMmRiZmE4OTYtN2QxNS00YmRiLWI2MTAtMWJkOGZlMGJhMzhlIiwiYXpwIjoiMTBhYjU0ZTktNWQ0MC00ZTExLWIzMzktNjhkYzQ4MzZmNzkxIiwidmVyIjoiMS4wIiwiaWF0IjoxNjIxODg2OTA0fQ.n0lsmEVUKWy3bpantYwneuFtACcOwOS4_gN1MlSj3bLAMOiRh4_0LAmgZ_9We0JaSth0DC6kL7cIYljswnypqQ8U-G0zmWODphBXI-iPX2atn-TAWXyLb8wdulX5sdHBG-0aKgyOxk0a91nxgQxgiCkJEzL5VqXfKKYlZNsuK8NxuSOvXql-jye94GfccEhES_N8mF3XHLJh-9eVbxHG5Wp3GE2TsFwX3F51Vz8rVfG6_Aru4SPy9oE2z5awNtXLofsDmDKlXyF2s-s-RMTBZfG3ElYeunRapLu3VQuz6_m9TaKUs5mTo1YpuIQhdkY5tRAPlQxY1V_fpEnNPYvNjQ',
        'User-Agent': 'Node',
      },
    }
  );

  const json = await response.json();
  return json.data;
}

module.exports = fetchApi;