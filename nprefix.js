const { getData } = require('./fetchconfig');

function runWhitoutPrefix(connection, database_name){
    
    /** QUESTIONS **/
    connection.query(`
        SELECT mq.id, category, questiontext as statement,hint, answer, length, (mq.qtype*1) as type, timecreated, timemodified
        FROM question mq 
        INNER JOIN question_hints mqh ON mq.id = mqh.questionid
        INNER JOIN question_answers  mqa ON mq.id = mqa.question;`, 
        (err, results) => {
        if (err) throw err;

        results.forEach(element => {
            let theQuery = `
            createQuestion(classroomId:1, input:{
                active: 1,
                alternatives: "",
                answer: "${element.answer}",
                chapterId: 1,
                hints: "${element.hint}",
                level: 1,
                order: 1,
                size: ${element.length},
                statement: "${element.statement}",
                statementUrl: "",
                type: ${element.type}
            }) {
                answer
            }`;
            const data = JSON.stringify({ query: `{${theQuery}}`});
            console.log(theQuery)
            // getData(data);
        });
    });

    /** SINGLE TASK **/
    connection.query(`
        SELECT id, name, course as roomid,Cast(allowsubmissionsfromdate as char(255)), Cast(duedate as char(255)),
        Cast(allowsubmissionsfromdate as char(255)) ,Cast(timemodified as char(255))
        FROM assign;`, 
        (err, results) => {
        if (err) throw err;

        results.forEach(element => {
            let theQuery = `
            createTaskType(classroomId:1, input:{
                classroomId: 1,
                hidden: true,
                name: "${element.name},"
                private: true,
                typegroupId: 1,
            }) {
                name
            }`;
            const data = JSON.stringify({ query: `{${theQuery}}`});
            console.log(theQuery);
            // getData(data);
        });
    });


    /** ROOM **/
    connection.query(`
        SELECT id,idnumber ,category, CONCAT(fullname,shortname) as name, Cast(timecreated as char(255)) as insertedAt, Cast(timemodified as char(255)) as updatedAt
        FROM course;`, 
        (err, results) => {
        if (err) throw err;

        results.forEach(element => {
            let theQuery = `
            createRoom(classroomId:1, input:{
                adminApproved: false,
                category: "${element.category}",
                code: "",
                configurations: "",
                hidden: false,
                locked: false,
                name: "${element.name}",
                privateCode: "",
                publicCode: "",
                subgroupId: 1,
            }) {
                name
            }`;
            const data = JSON.stringify({ query: `{${theQuery}}`});
            console.log(theQuery);
            // getData(data);
        });
    });


    /** TASKGROUP **/
    connection.query(`
    SELECT mg.id, courseid as roomid, name, Cast(mg.timecreated as char(255)),Cast(mg.timemodified as char(255)),
    mdu.id as userId, CONCAT(firstname, lastname)  
    FROM ${database_name}.groups mg 
    INNER JOIN groups_members mgm ON mg.id = mgm.groupid 
    INNER JOIN user mdu ON mgm.userid = mdu.id;`, 
    (err, results) => {
    if (err) throw err;

    let theQuery = '';
    results.forEach(element => {
    theQuery = theQuery.concat(`
    createTaskGroup (classroomId:1, input:{
        members: "",
        name: "${element.name}",
        roomId: 1,
        userId: ${element.userId}
    }) {
        name
    },`)
    });
    const data = JSON.stringify({ query: `{${theQuery}}`});
    // getData(data);
    console.log(theQuery);
    });



    /** POST QUERIES **/
    connection.query(`
        SELECT id, userid, courseid, CONCAT(subject,summary,content) as description , lastmodified, created,
        0, 0, 0, 0, '', 0, 0, 0, 0, '', 0
        FROM post;`,
        (err, results) => {
        if (err) throw err;

        results.forEach(item => {
            let theQuery = `
            createPost(classroomId:1, input: {
                active: false,
                backgroundId: 0,
                category: 0,
                classroomId: ${item.courseid},
                description: "${item.description}",
                isVideo: false,
                privacy: false,
                url: "",
                userid: ${item.userid}
            }) {
                description
            }`;
            const data = JSON.stringify({ query: `{${theQuery}}`});
            console.log(theQuery);
            //getdata(data)
        });
    });

    /** EVENT **/
    connection.query(`
        SELECT id, description as data, timestart, timemodified,
        0, 0, '', ''
        FROM event;`,
        (err, results) => {
        if (err) throw err;

        results.forEach( item => {
            let theQuery = `
            createEvent(classroomId:1, input: {
                calendarId: 1,
                data: "${item.data}",
                options: "",
                schedule: "",
            }){
                data
            }`;
            const data = JSON.stringify({ query: `{${theQuery}}`});
            console.log(theQuery);
            // getdata(data)
        });
    });


    /** USER_EVENT **/
    connection.query(`
        SELECT id, userid,CONCAT('nombre: ',name,' ,eventTipo: ',eventtype,' ,insertedAt: ',Cast(timestart as char(255)), ' ,updatedAt: ',Cast(timemodified as char(255))) as options
        FROM event evnt;`,
        (err, results) => {
        if (err) throw err;

        results.forEach(element => {
            let theQuery = `
            createUserEvent(classroomId:1, input:{
                calendarId: 1,
                eventId: ${element.id},
                options: "${element.options}",
                permissionEvent: "",
                userid: ${element.userid}
            }) {
                insertedAt
            }`;
            const data = JSON.stringify({ query: `{${theQuery}}`});
            console.log(theQuery);
            // getData(data);
        });
    });

    /** USER **/
    connection.query(`
        SELECT usr.id, auth, confirmed, firstname, idNUmber*1, lastname, email, phone1, address,
        timecreated, CONVERT(usr.timemodified, CHAR), imagealt, rl.shortname, 0
        FROM user usr
        INNER JOIN  role_assignments mra ON usr.id = mra.userid
        INNER JOIN  role rl ON rl.id = mra.roleid;`,
        (err, results) => {
        if (err) throw err;

        results.forEach(element => {
            let theQuery = `
            createAzUser(classroomId:1, input:{
                documentNumber:1 ,
                documentType: 1,
                email: "${element.email}",
                fatherName: "${element.lastname}",
                firstName: "${element.firstname}",
                motherName: "${element.lastname}" 
            }) {
                firstName
            }`;
            const data = JSON.stringify({ query: `{${theQuery}}`});
            console.log(theQuery);
            // getData(data);
        });
    });

    connection.end();
}

module.exports = runWhitoutPrefix;