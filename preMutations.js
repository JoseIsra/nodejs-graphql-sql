const fetchApi = require('./fetchconfig');
const getLoop = require('./callLoop');

let theData = {
    idClasroom: "",
    idService: "", 
    idContentGroup: "", 
    idContent: [],
    idChapter: [],
    idRoom: "",
    idUser: "",
    idGroup: "",
    idSubGroup:"",
    idEvent:"",
    idCalendar: ""
}

async function preMutations(connection, dataUser, teachers){
    let clasroomMutation = `
    {
        createClassroom(input: {
          active : true,
          colors: "blue, orange",
          membership: "1",
          name: "classroom_test",
          names: "",
          option: "{}",
          url: ""
        }){
          id
        }
    }`;
    const classroomQuery = JSON.stringify({ query: `${clasroomMutation}`});
    const classroomData = await fetchApi(classroomQuery);
    const idClasroom = classroomData['createClassroom'].id;
    

    //SERVICE
    let serviceMutation = `
    {
        createService(classroomId: ${idClasroom}, input: {
            classroomId: ${idClasroom},
            configurations: "",
            description: "",
            hidden: 1,
            image: "",
            name: "service_test"
            paymentMethods: "",
            pricing: "",
            secret: 1,
            teachers: ${teachers},
        }){
          id
        }
    }`;
    const serviceQuery = JSON.stringify({ query: `${serviceMutation}`});
    const serviceData = await fetchApi(serviceQuery);
    const idService = serviceData['createService'].id;


    // CONTENTGROUP
    let contentGroupMutation = `
    {
      createContentgroup(classroomId: ${idClasroom}, input: {
        name: "contentgroup_test",
        order: 1,
        serviceId: ${idService}
      }){
        id
      }
    }`;
    const contentGroupQuery = JSON.stringify({ query: `${contentGroupMutation}`});
    const contentgroupData = await fetchApi(contentGroupQuery);
    const idContentGroup = contentgroupData['createContentGroup'].id;

    // USER
    let userMutation = `
    {
      createAzUser(classroomId: ${idClasroom}, input: {
        documentNumber: 1,
        documentType: 1,
        email: "${dataUser.email}",
        fatherName: "${dataUser.lastname}",
        firstName: "${dataUser.firstName}",
        motherName: "${dataUser.lastname}"
        }){
            id
        }
    }`;

    const userQuery = JSON.stringify({ query: `${userMutation}`});
    const userData = await fetchApi(userQuery);
    const idUser = userData['createAzUser'].id;

    // GROUP
    let groupMutation =`
    {
      createGroup(classroomId:${idClasroom}, input: {
        category: 1,
        classroomId: ${idClasroom},
        hidden: false,
        name: "group_test"
      }) {
        id
      }
    }`

    const groupQuery = JSON.stringify({ query: `${groupMutation}`});
    const groupData = await fetchApi(groupQuery);
    const idGroup = groupData['createGroup'].id;

    //SUBGROUP
    let subgroupMutation =`
    {
      createSubgroup(classroomId:${idClasroom}, input: {
        groupId: ${idGroup},
        hidden: false,
        name: "subgroup_test"
      }) {
        id
      }
    }`

    const subgroupQuery = JSON.stringify({ query: `${subgroupMutation}`});
    const subgroupData = await fetchApi(subgroupQuery);
    const idSubGroup = subgroupData['createSubgroup'].id;

    // ROOM
    let roomMutation =`
    {
      createRoom(classroomId:${idClasroom}, input: {
        adminApproved: true,
        code: "",
        configurations: "",
        hidden: false,
        locked: false,
        name: "",
        privateCode: "",
        publicCode: "",
        subgroupId: ${idSubGroup}
      }) {
        id
      }
    }`

    const roomQuery = JSON.stringify({ query: `${roomMutation}`});
    const roomData = await fetchApi(roomQuery);
    const idRoom = roomData['createRoom'].id;


    // CALENDAR
    let calendarMutation =`
    {
      createCalendar(classroomId:${idClasroom}, input: {
        description: "",
        optionVisibility: "",
        options: "",
        title: "calendar_test",
        userId: ${idUser}
      }) {
        id
      }
    }`;

    const calendarQuery = JSON.stringify({ query: `${calendarMutation}`});
    const calendarData = await fetchApi(calendarQuery);
    const idCalendar = calendarData['createCalendar'].id;

    // CONTENT AND CHAPTER
    // let chapterNContent = await getLoop(connection, idClasroom, idContentGroup);

    theData.idClasroom = idClasroom;
    theData.idService = idService;
    theData.idContentGroup = idContentGroup;
    //theData.idContent = chapterNContent.idContentArray;    
    //theData.idChapter = chapterNContent.idChapterArray;
    theData.idUser = idUser;
    theData.idSubGroup = idSubGroup;
    theData.idRoom = idRoom;
    theData.idCalendar = idCalendar;

    return theData;
}

module.exports = preMutations;
